﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webstore.Models;
using webstore.ViewModels;

namespace webstore.Controllers
{
    public class ReportController : Controller
    {
        private readonly IItemRepository _itemRepository;

        public ReportController(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }
        public ViewResult Index()
        {
            ItemListViewModel itemList = new ItemListViewModel();
            itemList.Items = _itemRepository.Items;
            return View(itemList);

        }
}
}

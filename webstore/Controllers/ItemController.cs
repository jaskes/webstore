﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webstore.Models;
using webstore.ViewModels;

namespace webstore.Controllers
{
    public class ItemController : Controller
    {
        private readonly IItemRepository _itemRepository;
        private readonly ICategoryRepository _categoryRepository;

        public ItemController (IItemRepository itemRepository, ICategoryRepository categoryRepository)
        {
            _itemRepository = itemRepository;
            _categoryRepository = categoryRepository;
        }

        public IActionResult List() => View();

        public ViewResult FilteredList(string category)
        {
            IEnumerable<Item> items;
            string currentCategory = string.Empty;

            if (string.IsNullOrEmpty(category))
            {
                items = _itemRepository.Items.OrderBy(p => p.ItemID);
                currentCategory = "All pies";
            }
            else
            {
                items = _itemRepository.Items.Where(p => p.Category.CategoryName == category).OrderBy(p => p.ItemID);
                currentCategory = _categoryRepository.Сategories.FirstOrDefault(c => c.CategoryName == category).CategoryName;
            }

            return View(new ItemListViewModel
            {
                Items = items,
                CurrentCategory = currentCategory
            });
        }

        public IActionResult Details(int id)
        {
            var item = _itemRepository.GetItembyID(id);
            if (item == null)
                return NotFound();
            _itemRepository.IncrementVisitCounter(id);
            return View(item);
        }
        /*
        ItemListViewModel itemList = new ItemListViewModel();
        itemList.items = _ItemRepository.Items;
        itemList.currentCategory = "One point two";
        //ViewBag.CurrentCategory = "One point two";
        return View(itemList);
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }*/
    }
}

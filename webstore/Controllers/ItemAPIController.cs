﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webstore.Models;
using webstore.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webstore.Controllers
{
    [Route("api/[controller]")]
    public class ItemAPIController : Controller
    {
        private readonly IItemRepository _itemRepository;

        public ItemAPIController(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        [HttpGet]
        public IEnumerable<ItemViewModel> LoadMoreItems()
        {
            IEnumerable<Item> dbItems = null;

            dbItems = _itemRepository.Items.OrderBy(p => p.ItemID).Take(10);

            List<ItemViewModel> items = new List<ItemViewModel>();

            foreach (var dbItem in dbItems)
            {
                items.Add(MapDbItemToItemViewModel(dbItem));
            }
            return items;
        }

        private ItemViewModel MapDbItemToItemViewModel(Item dbItem)
        {
            return new ItemViewModel()
            {
                ItemID = dbItem.ItemID,
                Name = dbItem.Name,
                Price = dbItem.Price,
                ShortDescription = dbItem.ShortDescription,
                ThumbnailUrl = dbItem.ThumbnailUrl
            };
        }

    }
}

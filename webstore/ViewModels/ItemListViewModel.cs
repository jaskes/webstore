﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webstore.Models;

namespace webstore.ViewModels
{
    public class ItemListViewModel
    {
        public IEnumerable<Item> Items { get; set; }
        public string CurrentCategory { get; set; }

    }
}

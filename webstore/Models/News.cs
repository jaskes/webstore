﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class News
    {
        public int NewsId { get; set; }
        public string Header { get; set; }
        public string Text { get; set; }
        public string ImageURL { get; set; }
        public int VisitCount { get; set; }
    }
}
 
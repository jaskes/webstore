﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public static class storeDbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            storeDbContext context = applicationBuilder.ApplicationServices.GetRequiredService<storeDbContext>();

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(Categories.Select(c => c.Value));
            }

            if (!context.Items.Any())
            {
                context.AddRange
                (
                    //new Item { Name = "", Price = 1.01M, ShortDescription = "", LongDescription = "", Category = Categories[""], ImageURL = "", InStock = true, SpecialOffer = true, ThumbnailUrl = "" }
                    new Item { Name = "An item", Price = 12.95M, ShortDescription = "Our famous whatever it is!", LongDescription = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc.", Category = Categories["number one"], ImageURL = "https://cdpiz1.pizzasoft.ru/pizzafab/items/0/po-domashnemu-bolshaya-large-4-97879.jpg", InStock = true, SpecialOffer = true, ThumbnailUrl = "https://cdpiz1.pizzasoft.ru/pizzafab/items/0/po-domashnemu-bolshaya-small-4-52328.jpg" }
                );
            }
            context.SaveChanges();
        }

        private static Dictionary<string, Category> categories;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (categories == null)
                {
                    var catList = new Category[]
                    {
                        new Category { CategoryName = "number one" },
                        new Category { CategoryName = "number two" },
                        new Category { CategoryName = "number three" }
                    };

                    categories = new Dictionary<string, Category>();

                    foreach (Category cat in catList)
                    {
                        categories.Add(cat.CategoryName, cat);
                    }
                }

                return categories;
            }
        }
    }
}

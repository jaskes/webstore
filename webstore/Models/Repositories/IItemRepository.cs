﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public interface IItemRepository
    {
        IEnumerable<Item> Items { get; }
        IEnumerable<Item> SpecialOffer { get; }

        Item GetItembyID(int itemID);
        void IncrementVisitCounter(int itemID);
    }
}

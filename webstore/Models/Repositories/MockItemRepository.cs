﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class MockItemRepository :IItemRepository
    {
        private readonly ICategoryRepository _categoryRepository = new MockCategoryRepository();

        public IEnumerable<Item> Items
        {
            get
            {
                return new List<Item>
                {
                    new Item{ ItemID=1, Name="Generic", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=1, Name="An Item", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=1, Name="Something else", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=1, Name="And more", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=1, Name="one more", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=1, Name="a piece of something", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=1, Name="boo", ShortDescription="Some generic information", Price=18, Category = _categoryRepository.Сategories.ToList()[0]},
                    new Item{ ItemID=2, Name="hello", ShortDescription="Some another information", Price=83, Category = _categoryRepository.Сategories.ToList()[1]},
                    new Item{ ItemID=3, Name="Boose", ShortDescription="Quality boose", Price=95, Category = _categoryRepository.Сategories.ToList()[2]}
                };
            }
        }

        public IEnumerable<Item> SpecialOffer { get; }

        //public IEnumerable<Item> featured => throw new NotImplementedException();
        //public IEnumerable<Item> items => throw new NotImplementedException();

        public Item GetItembyID(int itemID)
        {
            throw new NotImplementedException();
        }

        public void IncrementVisitCounter(int itemID)
        {
            throw new NotImplementedException();
        }
    }
}

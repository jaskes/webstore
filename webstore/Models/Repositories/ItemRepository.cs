﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class ItemRepository : IItemRepository
    {

        private readonly storeDbContext _storeDbContext;

        public ItemRepository(storeDbContext storeDbContext)
        {
            _storeDbContext = storeDbContext;
        }

        public IEnumerable<Item> Items
        {
            get
            {
                return _storeDbContext.Items.Include(c => c.Category);
            }
        }

        public IEnumerable<Item> SpecialOffer
        {
            get
            {
                return _storeDbContext.Items.Include(c => c.Category).Where(p => p.SpecialOffer);
            }
        }

        public Item GetItembyID(int itemID)
        {
            return _storeDbContext.Items.FirstOrDefault(i => i.ItemID == itemID);
        }

        public void IncrementVisitCounter(int itemID)
        {
            _storeDbContext.Items.FirstOrDefault(i => i.ItemID == itemID).VisitCount++;
            _storeDbContext.SaveChanges();
        }

        public void IncrementPurchaseCounter(int itemID)
        {
            _storeDbContext.Items.FirstOrDefault(i => i.ItemID == itemID).PurchaseCount++;
            _storeDbContext.SaveChanges();
        }
    }
}

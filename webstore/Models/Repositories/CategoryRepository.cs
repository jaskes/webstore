﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly storeDbContext _storeDbContext;

        public CategoryRepository(storeDbContext storeDbContext)
        {
            _storeDbContext = storeDbContext;
        }

        public IEnumerable<Category> Сategories => _storeDbContext.Categories;

        public Item GetCategorybyID(int itemID)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class MockCategoryRepository :ICategoryRepository
    {
        public IEnumerable<Category> Сategories
        {
            get
            {
                return new List<Category>
                {
                    new Category{ CategoryID=1, CategoryName="Generic", Description="Some generic information"},
                    new Category{ CategoryID=2, CategoryName="Another", Description="Some generic information"},
                    new Category{ CategoryID=3, CategoryName="And a new one", Description="Some generic information"}
                };
            }
        }

        public Item GetCategorybyID(int itemID)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class OrderRepository : IOrderRepository
    {
        private readonly storeDbContext _storeDbContext;
        private readonly ShoppingCart _shoppingCart;


        public OrderRepository(storeDbContext storeDbContext, ShoppingCart shoppingCart)
        {
            _storeDbContext = storeDbContext;
            _shoppingCart = shoppingCart;
        }


        public void CreateOrder(Order order)
        {
            order.OrderPlaced = DateTime.Now;

            _storeDbContext.Orders.Add(order);

            var shoppingCartItems = _shoppingCart.ShoppingCartItems;

            foreach (var shoppingCartItem in shoppingCartItems)
            {
                var orderDetail = new OrderDetail()
                {
                    Amount = shoppingCartItem.Amount,
                    ItemID = shoppingCartItem.Item.ItemID,
                    OrderID = order.OrderID,
                    Price = shoppingCartItem.Item.Price
                };

                _storeDbContext.OrderDetails.Add(orderDetail);
            }

            _storeDbContext.SaveChanges();
        }
    }
}

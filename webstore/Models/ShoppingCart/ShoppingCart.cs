﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webstore.Models
{
    public class ShoppingCart
    {
        private readonly storeDbContext _storeDbContext;
        private ShoppingCart(storeDbContext storeDbContext)
        {
            _storeDbContext = storeDbContext;
        }

        public string ShoppingCartId { get; set; }

        public List<ShoppingCartItem> ShoppingCartItems { get; set; }

        public static ShoppingCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;

            var dbcontext = services.GetService<storeDbContext>();
            string cartId = session.GetString("CartId") ?? Guid.NewGuid().ToString();

            session.SetString("CartId", cartId);

            return new ShoppingCart(dbcontext) { ShoppingCartId = cartId };
        }

        public void AddToCart(Item item, int amount)
        {
            var shoppingCartItem =
                    _storeDbContext.ShoppingCartItems.SingleOrDefault(s => s.Item.ItemID == item.ItemID && s.ShoppingCartId == ShoppingCartId);

            if (shoppingCartItem == null)
            {
                shoppingCartItem = new ShoppingCartItem
                {
                    ShoppingCartId = ShoppingCartId,
                    Item = item,
                    Amount = 1
                };

                _storeDbContext.ShoppingCartItems.Add(shoppingCartItem);
            }
            else
            {
                shoppingCartItem.Amount++;
            }
            _storeDbContext.SaveChanges();
        }

        public int RemoveFromCart(Item item)
        {
            var shoppingCartItem =
                    _storeDbContext.ShoppingCartItems.SingleOrDefault(s => s.Item.ItemID == item.ItemID && s.ShoppingCartId == ShoppingCartId);

            var localAmount = 0;

            if (shoppingCartItem != null)
            {
                if (shoppingCartItem.Amount > 1)
                {
                    shoppingCartItem.Amount--;
                    localAmount = shoppingCartItem.Amount;
                }
                else
                {
                    _storeDbContext.ShoppingCartItems.Remove(shoppingCartItem);
                }
            }

            _storeDbContext.SaveChanges();

            return localAmount;
        }

        public List<ShoppingCartItem> GetShoppingCartItems()
        {
            return ShoppingCartItems ??
                   (ShoppingCartItems = _storeDbContext.ShoppingCartItems.Where(c => c.ShoppingCartId == ShoppingCartId).Include(s => s.Item).ToList());
        }

        public void ClearCart()
        {
            var cartItems = _storeDbContext.ShoppingCartItems.Where(cart => cart.ShoppingCartId == ShoppingCartId);

            _storeDbContext.ShoppingCartItems.RemoveRange(cartItems);

            _storeDbContext.SaveChanges();
        }



        public decimal GetShoppingCartTotal()
        {
            var total = _storeDbContext.ShoppingCartItems.Where(c => c.ShoppingCartId == ShoppingCartId).Select(c => c.Item.Price * c.Amount).Sum();
            return total;
        }
    }
}

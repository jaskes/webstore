﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace webstore.Models
{
    public class storeDbContext: IdentityDbContext<Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser>
    {
        public storeDbContext(DbContextOptions<storeDbContext> options) : base(options){}
        public DbSet<Category> Categories { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
    }
}
